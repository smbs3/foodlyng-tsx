type Props = {
  className: string;
  onClick?: ()=>void;
};

const Icon = ({ className, onClick }: Props) => {
  return <i className={className} onClick={onClick} />;
};

export default Icon;
