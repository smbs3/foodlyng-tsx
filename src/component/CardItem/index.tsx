import { useContext } from "react";
import { FoodContext } from "../../context/FoodContext";
import { ProductI } from "../../utils/map.typing";
import Icon from "../Icon";
import Image from "../Image";

interface Props {
  product: ProductI;
  onDelete: (id: string, productId: string) => void;
}

const Cart = (props: Props) => {
  const { product, onDelete } = props;
  const context = useContext(FoodContext);
  return (
    <div className="CartProduct">
      <div className="CartProduct-info">
        <Image src={product.image} className="Cart-image" />
        <div className="Cart-content">
          <p className="Cart-name">{product.name}</p>
          <span className="Cart-price">1 x ${product.price}</span>
        </div>
      </div>
      <Icon
        className="fa-solid fa-trash-can Cart-delete"
        onClick={() => onDelete(context?.cart?.id as string, product.id)}
      />
    </div>
  );
};

export default Cart;
