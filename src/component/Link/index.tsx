import { PropsWithChildren } from "react";

interface Props {
  className: string;
  href?: string,
};

const Link = (props: PropsWithChildren<Props>) => {
  return (
    <a className={props.className} href={props.href}>{props.children}</a>
  )
}

export default Link;