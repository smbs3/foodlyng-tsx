import { PropsWithChildren } from "react";

interface Props {
  className: string;
  onClick?: ()=> void;
};

const Button = (props: PropsWithChildren<Props>) => {
  return (
    <button className={props.className} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default Button;
