import React, { Fragment } from "react";
import Icon from "../Icon";
import { v4 as uuidv4 } from "uuid";

interface Props {
  options?: string[];
  text: string;
}

const DropdownMenu = (props: Props) => {
  const { options, text } = props;
  const renderIcon = () => {
    if (options?.length) return <Icon className="fa-solid fa-angle-down" />;
    return <Fragment></Fragment>;
  };

  const renderOptions = () => {
    if (!options?.length) return <Fragment></Fragment>;
    return (
      <ul className="BarSubItem">
        {options?.map((option) => (
          <li className="BarSubItem-li" key={uuidv4()}>
            {option}
          </li>
        ))}
      </ul>
    );
  };

  const renderText = () => {
    if (options?.length) return text;
    return <li className="Bar-link">{text}</li>;
  };

  return (
    <li className="Bar-item">
      {renderText()} {renderIcon()}
      {renderOptions()}
    </li>
  );
};

export default DropdownMenu;
