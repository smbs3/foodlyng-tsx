import  { useState } from "react";
import Button from "../Button";
import Card from "../CardProduct";
import useAxios from "axios-hooks";
import Section from "../Section";
import { v4 as uuidv4 } from 'uuid';
import { Category, ProductI } from "../../utils/map.typing";

const Products = () => {

  const [active, setActive] = useState("");

  const [{ data }, refetch] = useAxios("/products");

  const filterList =  (category:string) => async ()=> {
    await refetch({
      params: {
        category,
      },
    });
    setActive(category);
  };


  const renderProducts = () => {
    return data?.map((product:ProductI) => (
      <Card
      key={uuidv4()}
        product={product}
      ></Card>
    ));
  };

  const getIsActive = (label: string) => {
    if (active === label) return "active";
    return "";
  };

  return (
    <Section title="Our Featured Items" text="Our most popular items">
      <div className="Product-categories">
        <Button
          className={`Product-tab ${getIsActive("")}`}
          onClick={filterList("")}
        >
          All Categories
        </Button>
        <Button
          className={`Product-tab ${getIsActive("NOODLES")}`}
          onClick={filterList(Category.NOODLES)}
        >
          Noodles
        </Button>
        <Button
          className={`Product-tab ${getIsActive("BURGER")}`}
          onClick={filterList(Category.BURGER)}
        >
          Burger
        </Button>
        <Button
          className={`Product-tab ${getIsActive("CHICKEN")}`}
          onClick={filterList(Category.CHICKEN)}
        >
          Chicken
        </Button>
        <Button
          className={`Product-tab ${getIsActive("ICE_CREAM")}`}
          onClick={filterList(Category.ICE_CREAM)}
        >
          Ice Cream
        </Button>
        <Button
          className={`Product-tab ${getIsActive("DRINKS")}`}
          onClick={filterList(Category.DRINKS)}
        >
          Drinks
        </Button>
      </div>
      <div className="Product-item">{renderProducts()}</div>
    </Section>
  );
};

export default Products;
