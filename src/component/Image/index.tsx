interface Props {
  className: string;
  src: string;
  alt?: string;
};

const Image = (props:Props) => {
  const { src, alt, className } = props
  return <img src={src} alt={alt} className={className} />;
};

export default Image;
