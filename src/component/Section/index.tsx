import { PropsWithChildren } from "react";

interface Props {
    title: string;
    text: string;
}

const Section = (props: PropsWithChildren<Props>) => {
    
    return (
        <section className="Section">
        <p className="Section-heading">{props.title}</p>
        <h3 className="Section-title">{props.text}</h3>
        {props.children}
        </section>
    );
}

export default Section;
