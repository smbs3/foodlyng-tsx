import { useContext, useEffect, useState, useMemo } from "react";
import Icon from "../Icon";
import Button from "../Button";
import { Dock } from "react-dock";
import { FoodContext } from "../../context/FoodContext";
import Card from "../CardItem";
import Dropdown from "../DropdownMenu";
import { blogSubMenu, foodSubMenu, optionSubMenus } from "../../utils/data";
import { useInterval } from "../../hooks/useInterval";
import { ProductI } from "../../utils/map.typing";


const Bar = () => {
 
  const [isVisible, setisVisible] = useState(false);
  const [products, setProducts] = useState<ProductI[]>([]);
  const [countProduct, setCountProduct] = useState<number>(0);

  const context = useContext(FoodContext);

  useEffect(() => {
    const fetchData = async () => {
      if (isVisible) {
        const cart: any = await context?.getCart();
        setProducts(cart.data.products);
      }
    };
    fetchData();
  }, [isVisible]);

  useInterval(async () => {
    const cart:any = await context?.getCart();
    if (cart) {
      setCountProduct(cart?.data?.products.length);
    }
  }, 2000);

  const handleChange = () => {
    setisVisible(!isVisible);
  };

  const subTotal = useMemo(() => {
    if (products.length === 0) return;
    let sum = 0;

    products.forEach((product) => {
      sum = sum + product.price;
    });
    return sum;
  }, [products]);

  const onDelete = async (cartId: string, productId: string) => {
    await context?.deleteProduct(cartId, productId);
    const response:any = await context?.getCart();
    if (response) setProducts(response?.data?.products);
  };

  return (
    <>
      <nav className="Bar">
        <div className="Bar-options">
          <img
            src="https://foodingly.netlify.app/assets/img/logo.png"
            alt="Foodingly Logo"
          />
          <ul className="Bar-menu">
            <Dropdown text="Home" />
            <Dropdown text="About Us" />
            <Dropdown text="Food Menu" options={foodSubMenu} />
            <Dropdown text="Blog" options={blogSubMenu} />
            <Dropdown text="Page" options={optionSubMenus} />
            <Dropdown text="Contact" />
          </ul>
        </div>
        <div className="Bar-buttons">
          <Icon
            className="fa-solid fa-bag-shopping Bar-icon"
            onClick={handleChange}
          />
          <span className="Bar-span">{countProduct}</span>
          <Dock
            position="right"
            isVisible={isVisible}
            size={0.165}
            fluid={true}
            dimMode="opaque"
            onVisibleChange={handleChange}
          >
            <div className="Cart-visible">
              <div className="Cart-container">
                <div className="Cart-header">
                  <p className="Cart-title">My Cart ({countProduct})</p>
                  <Icon className="fa-solid fa-xmark" onClick={handleChange} />
                </div>
                <div className="Cart-Body">
                  {products?.map((product) => (
                    <Card
                      product={product}
                      onDelete={onDelete}
                      key={product.id}
                    />
                  ))}
                </div>
                <div className="Cart-footer">
                  <p className="Cart-subtotal">Subtotal</p>
                  <span>${subTotal}</span>
                </div>
                <Button className="button slim" onClick={() => {}}>
                  Checkout
                </Button>
              </div>
            </div>
          </Dock>

          <Icon
            className="fa-solid fa-magnifying-glass Bar-icon green"
            onClick={() => {}}
          />
          <Button className="button Secondary" onClick={() => {}}>
            Reservation
          </Button>
        </div>
      </nav>
    </>
  );
};

export default Bar;
