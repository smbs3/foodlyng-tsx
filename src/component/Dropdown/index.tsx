import React, { useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import { AlignType } from "react-bootstrap/esm/types";

interface Props {
  options: [];
  item: [];
  toggleClass: string;
  itemClass: string;
  defaultValue: string;
  align: AlignType;
  menuHover: string;
}

const DropdownBar = ({
  options,
  toggleClass,
  itemClass,
  defaultValue,
  align,
  menuHover,
}: Props) => {
  const [value, setValue] = useState(defaultValue);

  const handleSelect = (item: string) => () => {
    setValue(item);
  };
  const renderItem = () => {
    return options.map((item) => (
      <Dropdown.Item
        className={itemClass}
        onClick={handleSelect(item)}
        key={item}
      >
        {item}
      </Dropdown.Item>
    ));
  };

  return (
    <Dropdown align={align}>
      <Dropdown.Toggle className={toggleClass}>{value}</Dropdown.Toggle>
      <Dropdown.Menu className={menuHover}>{renderItem()}</Dropdown.Menu>
    </Dropdown>
  );
};

export default DropdownBar;
