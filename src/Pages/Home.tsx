import Product from "../component/Home/Product";

const Home = () => {
  return (
    <main>
      <Product />
    </main>
  );
};

export default Home;
