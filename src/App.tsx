
import './scss/main.scss';
import Home from "./Pages/Home";
import Axios from "axios";
import { configure } from "axios-hooks";
import Bar from './component/Header/Bar';
import FoodProvider from "./context/FoodContext";

export default function App() {

  const axios = Axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    headers: {
      authorization: `Bearer ${import.meta.env.VITE_API_TOKEN}`
    }
  })
  
  configure({ axios })

  return (
    <FoodProvider>
    <Bar />
    <Home />
    </FoodProvider>
  );
}
