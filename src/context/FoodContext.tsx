import { createContext, PropsWithChildren, useState } from "react";
import useAxios from "axios-hooks";
import { CartI } from "../utils/map.typing";

interface FoodContextI {
  addProduct: (productId: string) => void;
  getCart: () => void;
  deleteProduct: (cartId: string, productId: string) => void;
  cart: CartI | null;
}

export const FoodContext = createContext<FoodContextI | null>(null);

const FoodProvider = (props: PropsWithChildren<any>) => {
  const [, refetch] = useAxios("/cart", { manual: true });
  const [cart, setCart] = useState<CartI | null>(null);

  const createCart = () =>
    refetch({
      method: "POST",
      data: {
        products: [],
      },
    });

  const addItemCart = (cartId: string, productId: string) =>
    refetch({
      method: "POST",
      url: "/cart/add-product",
      data: {
        cartId,
        productId,
      },
    });

  const getCart = () => {
    if (!cart) return;
    return refetch({
      method: "GET",
      url: `/cart/${cart?.id}`,
    });
  };

  const addProduct = async (productId: string) => {
    if (!cart) {
      const cartCreated = await createCart();
      setCart(cartCreated.data);

      return addItemCart(cartCreated.data.id, productId);
    }
    return addItemCart(cart.id, productId);
  };

  const deleteProduct = (cartId: string, productId: string) =>
    refetch({
      method: "POST",
      url: "/cart/remove-product",
      data: {
        cartId,
        productId,
      },
    });

  return (
    <FoodContext.Provider value={{ addProduct, getCart, deleteProduct, cart }}>
      {props.children}
    </FoodContext.Provider>
  );
};

export default FoodProvider;
