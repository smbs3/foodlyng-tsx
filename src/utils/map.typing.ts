export interface CartI {
    id: string;
    products: ProductI[];
  }
 export interface ProductI {
    id: string;
    name: string;
    price: number;
    category: Category;
    discount: number;
    image: string;
  }
  
 export enum Category {
    NOODLES = "NOODLES",
    CHICKEN = "CHICKEN",
    ICE_CREAM = "ICE_CREAM",
    BURGER = "BURGER",
    DRINKS = "DRINKS",
  }